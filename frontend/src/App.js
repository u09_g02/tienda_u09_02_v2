import React, {Fragment} from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';

import Menu from './components/Menu.jsx';
import Register from './components/Register.jsx';
import ProductoListar from './components/ProductoListar.jsx';
import ActualizarProducto from './components/ActualizarProducto.jsx';
import BorrarProducto from './components/BorrarProducto.js';
import AgregarProducto from './components/AgregarProducto.jsx';
import Login from './components/Login.jsx';

function App() {
  return (
    <main className='App'>
      <Fragment>
        <BrowserRouter>
          <Routes>
            <Route path='/' element={<Login />} exact></Route>
            <Route path='/menu' element={<Menu />} exact></Route>
            <Route path='/register' element={<Register />} exact></Route>
            <Route path='/productos' element={<ProductoListar />} exact></Route>
            <Route path='/productos/borrar/:id' element={<BorrarProducto />} exact></Route>
            <Route path='/productos/actualizar/:id' element={<ActualizarProducto />} exact></Route>
            <Route path='/productos/agregar' element={<AgregarProducto />} exact></Route>
          </Routes>    
        </BrowserRouter>
      </Fragment>
    </main>
  );
}

export default App;
