import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';

import Encabezado from './Encabezado.jsx';
import MenuLateral from './MenuLateral.jsx';


function ActualizarProducto(id){
    const params = useParams();
	const navigate = useNavigate();

    const [categoria, setCategoria] = useState('');
    const [nombre, setNombre] = useState('');
    const [marca, setMarca] = useState('');
    const [precio, setPrecio] = useState('');
    const [imagen, setImagen] = useState('');
    const [estado, setEstado] = useState('');

	// Peticion para autenticar
	// **************************************************************************************
	const token = localStorage.getItem('token');
	let bearer;
	if(token===''){
		bearer="";
	} else{
		bearer= bearer=`${token}`;
	}
	const config={headers:{'Content-Type': 'application/json','x-auth-token': bearer}}
	// **************************************************************************************

    // Obtencion de los datos del producto
    useEffect(() =>{
        axios.get(`https://tienda-u09-02-v2.herokuapp.com/api/v1/productos/cargar/${params.id}`, config).then(res => {
			const producto = res.data[0];

            // Se guardan los datos del producto en los hooks
            setCategoria(producto.categoria);
            setNombre(producto.nombre);
            setMarca(producto.marca);
            setPrecio(producto.precio);
            setImagen(producto.imagen);
            setEstado(producto.estado);
        }).catch(err => { console.log(err.stack); });
    }, []);

    // Actualizacion de datos
    function actualizarProducto(){

        const productoActualizar = {
            categoria: categoria,
            nombre: nombre,
            marca: marca,
            precio: precio,
            imagen: imagen,
            estado: estado
        }

		const config = {
            body: JSON.stringify(productoActualizar),
            headers: {'Content-Type': 'application/json', 'x-auth-token': bearer}
        }

        axios.put(`https://tienda-u09-02-v2.herokuapp.com/api/v1/productos/editar/${params.id}`, productoActualizar, config).then(res =>{
            Swal.fire({ position: 'center', icon: 'success',  title: 'El registro fue actualizado exitosamente!', showConfirmButton: false, timer: 1500 });
			navigate('/productos');
        }).catch(err => { console.log(err.stack); });
    }

	function retornar(){
		navigate('/menu');
	}

    return(
    <div className="sidebar-mini fixed">
    <div className="wrapper">
        <Encabezado/>
        <MenuLateral/>
        <div className="content-wrapper flex-row">
        <div className="container-fluid">
            <div className="row">
                <div className="col-sm-12 p-0">
                    <div className="main-header">
                        <h4>Productos</h4>
                        <ol className="breadcrumb breadcrumb-title breadcrumb-arrow">
                            <li className="breadcrumb-item">
                            <a href="/menu">
                                <i className="icofont icofont-home"></i>
                            </a>
                            </li>
                            <li className="breadcrumb-item"><a href="/menu">Productos</a>
                            </li>
                            <li className="breadcrumb-item"><a href="/menu">Lista De Productos</a>
                            </li>
                            <li className="breadcrumb-item"><a>Actualizar Producto</a>
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
            <section className="pcoded-main-container col-sm-12">
                <div className="pcoded-content col-sm-12">
                    <div className="col-sm-12">
                        <div className="card">

                        <div className="card-body ml-2">
                            <form>
                                <div className="form-group col-sm-12">
                                    <h2 className="mt-5">PRODUCTOS</h2>
                                </div>
                                <div className="form-group col-sm-12">
                                    <label htmlFor="categoria">Categoria</label>
                                    <input type="text" className="form-control"id="categoria" value={categoria} onChange={(e)=>{setCategoria(e.target.value)}} placeholder="Categoria"></input>
                                </div>
                                <div className="form-group col-sm-12">
                                    <label htmlFor="nombre">Nombre</label>
                                    <input type="text" className="form-control" id="nombre" value={nombre} onChange={(e)=>{setNombre(e.target.value)}} placeholder="Nombre"></input>
                                </div>
                                <div className="form-group col-sm-12">
                                    <label htmlFor="nombre">Marca</label>
                                    <input type="text" className="form-control" id="marca" value={marca} onChange={(e)=>{setMarca(e.target.value)}} placeholder="Marca"></input>
                                </div>
                                <div className="form-group col-sm-12">
                                    <label htmlFor="precio">Precio</label>
                                    <input type="text" className="form-control" id="precio" value={precio} onChange={(e)=>{setPrecio(e.target.value)}}  placeholder="Precio"></input>
                                </div>
                                <div className="form-group col-sm-12">
                                    <label htmlFor="activo">Stock</label>
                                    <input type="text" className="form-control" id="activo" value={estado} onChange={(e)=>{setEstado(e.target.value)}} placeholder="Activo"></input>
                                </div>
                                <div className="form-group col-sm-12">
                                    <button type="button" className="btn btn-info" onClick={ retornar }>Regresar</button>
                                    <button type="button" className="btn btn-success" onClick={ actualizarProducto }>Actualizar</button>
                                </div>
                            </form>
                        </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        </div>
    </div>
    </div>
    )
}

export default ActualizarProducto;