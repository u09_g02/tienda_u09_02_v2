import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import BorrarProducto from "./BorrarProducto";

function ProductoListar(){
    const [dataProductos, setDataProductos] = useState([]);

	// Peticion para autenticar
	// **************************************************************************************
	const token = localStorage.getItem('token');
	let bearer;
	if(token===''){
		bearer="";
	} else{
		bearer= bearer=`${token}`;
	}
	const config={headers:{'Content-Type': 'application/json','x-auth-token': bearer}}
	// **************************************************************************************

	useEffect(()=>{
        axios.get('https://tienda-u09-02-v2.herokuapp.com/api/v1/productos/listar', config).then(res => {
			setDataProductos(res.data)
        }).catch(err=>{console.log(err.stack)})
    },[])


	//Estructura para dibujar fondos de diversos colores en la tabla
	const tabla = document.getElementsByTagName("tr");
	//tabla[i].style.backgroundColor = "#888888";
	let i, j=0, fila=[], color=[], inicio=0;
	color[0]="bg-info";color[1]="";color[2]="bg-success";color[3]="";color[4]="bg-danger";color[5]="";color[6]="bg-warning";color[7]="";color[8]="bg-active";color[9]="";
	for(i=0;i<tabla.length;i++)
	{
		fila[i]=color[j];
		j++;
		if(j===10){j=0;}
	}


    return(
		<div className="content-wrapper">
			<div className="container-fluid">
				<div className="row">
					<div className="col-sm-12 p-0">
						<div className="main-header">
							<h4>Productos</h4>
							<ol className="breadcrumb breadcrumb-title breadcrumb-arrow">
								<li className="breadcrumb-item">
								<a href="/menu">
									<i className="icofont icofont-home"></i>
								</a>
								</li>
								<li className="breadcrumb-item"><a href="/menu">Productos</a>
								</li>
								<li className="breadcrumb-item"><a href="/menu">Lista De Productos</a>
								</li>
							</ol>
						</div>
					</div>
				</div>


				<div className="row">
					<div className="col-sm-12">
						<div className="card">
							<div className="card-header">
								<h5 className="card-header-text">Lista De Productos</h5>
							</div>
							<div className="card-block">
								<div className="row">
								<div className="col-sm-12 table-responsive">
									<table className="table table-inverse">
										<thead>
										<tr key={-1} className="bg-active">
											<td colSpan={8} align="right">
												<Link to={`/productos/agregar`} className="btn btn-success btn-icon waves-effect waves-light">
												<i className="icofont icofont-ui-add"></i>
												</Link>
											</td>
										</tr>
										<tr key={0}>
											<td align="center">Id</td>
											<td align="center">Categoria</td>
											<td align="center">Nombre</td>
											<td align="center">Marca</td>
											<td align="center">Precio</td>
											<td align="center">Disponibilidad</td>
											<td align="center">Editar</td>
											<td align="center">Eliminar</td>
										</tr>
										</thead>
										<tbody>
										{
											dataProductos.map((producto,indice) => (
											<tr className={`${fila[indice]}`} key={producto.id}>
												<td align="center">{producto.id}</td>
												<td align='center'>{producto.categoria}</td>
												<td align="center">{producto.nombre}</td>
												<td align="center">{producto.marca}</td>
												<td align="center">{producto.precio}</td>
												<td align="center">{producto.estado ? 'Activo' : 'Inactivo'}</td>
												<td align="center"><Link to={`/productos/actualizar/${producto.id}`} className="btn btn-primary btn-icon waves-effect waves-light"><i className="icofont icofont-edit"></i></Link></td>
												<td align="center">
													<button type="button" onClick={()=>{BorrarProducto(producto.id)}} className="btn btn-danger btn-icon waves-effect waves-light" data-toggle="tooltip" data-placement="top" title=".icofont-code-alt">
														<i className="icofont icofont-trash"></i>
													</button>
												</td>
											</tr>
										))
										}
										</tbody>
									</table>
								</div>
								</div>
							</div>
						</div>

					</div>
				</div>


			</div>
		</div>
	)
}

export default ProductoListar;


/*<div>
	<h1 align="left">Lista de Productos</h1> 
	<table className="table table-striped table-bordered">
		<thead>
		<tr key={-1}>
			<td colSpan={7} align="right"><Link to={`/productos/add`}><li className='btn btn-success'>Agregar Pedido</li></Link></td>
		</tr>
			<tr key={0} className="table-primary">
				<th scope="col">Id</th>
				<th scope="col">Categoria</th>
				<th scope="col">Nombre</th>
				<th scope="col">Marca</th>
				<th scope="col">Precio</th>
				<th scope="col">Estado</th>
				<th scope="col">Acciones</th>
			</tr>
		</thead>
			<tbody>
				{dataProductos.map( producto => (
					<tr key={producto.id}>
						<th scope="row">{producto.id}</th>
						<td align="center">{producto.categoria}</td>
						<td align="center">{producto.nombre}</td>
						<td align="center">{producto.marca}</td>
						<td align="right">{`$ ${ producto.precio }`}</td>
						<td align="center">{producto.estado ? "Disponible" : "Sin Stock"}</td>
						<td><Link to={`/productos/update/${producto.id}`} ><li className="btn btn-success btn-actualizar">Editar</li></Link>
								<button className="btn btn-danger btn-borrar" onClick={() => {BorrarProducto(producto.id, direccion)} }>Borrar</button>
						</td>           
					</tr>
					))	
				}
			</tbody>
	</table> 
	
</div>*/