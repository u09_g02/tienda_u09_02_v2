import React, { useState } from 'react';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import uniquid from 'uniqid';
import Encabezado from './Encabezado.jsx';
import MenuLateral from './MenuLateral.jsx';

function AgregarProducto(){

    const navigate = useNavigate();

    const [categoria, setCategoria] = useState('');
    const [nombre, setNombre] = useState('');
    const [marca, setMarca] = useState('');
    const [precio, setPrecio] = useState('');
    const [imagen, setImagen] = useState('');
    const [estado, setEstado] = useState('');

    // Peticion para autenticar
    // **************************************************************************************
    const token = localStorage.getItem('token');
    let bearer;
    if(token===''){
        bearer="";
    } else{
        bearer= bearer=`${token}`;
    }
    const config={headers:{'Content-Type': 'application/json','x-auth-token': bearer}}
    // **************************************************************************************

    function agregarProducto(){

        const productoNuevo = {
                id: uniquid(),
                categoria:categoria,
                nombre:nombre,
                marca: marca,
                precio: precio,
                imagen: imagen,
                estado: estado
            }

        const config = {
            body: JSON.stringify(productoNuevo),
            headers: {'Content-Type': 'application/json', 'x-auth-token': bearer}
        }

        axios.post(`https://tienda-u09-02-v2.herokuapp.com/api/v1/productos/agregar`, productoNuevo, config).then(res => {
            Swal.fire({ position: 'center', icon: 'success',  title: 'El registro fue agregado exitosamente!', showConfirmButton: false, timer: 1500 })
            navigate('/menu')
        }).catch(err => { console.log(err.stack); })
    }

    function regresar(){
        navigate('/menu');
    }

    return(
        <div className="sidebar-mini fixed">
        <div className="wrapper">
            <Encabezado/>
            <MenuLateral/>
            <div className="content-wrapper flex-row">
            <div className="container-fluid">
				<div className="row">
					<div className="col-sm-12 p-0">
						<div className="main-header">
							<h4>Productos</h4>
							<ol className="breadcrumb breadcrumb-title breadcrumb-arrow">
								<li className="breadcrumb-item">
								<a href="/menu">
									<i className="icofont icofont-home"></i>
								</a>
								</li>
								<li className="breadcrumb-item"><a href="/menu">Productos</a>
								</li>
								<li className="breadcrumb-item"><a href="/menu">Lista De Productos</a>
								</li>
                                <li className="breadcrumb-item"><a href="/productos/agregar">Agregar Producto</a>
								</li>
							</ol>
						</div>
					</div>
				</div>
                <section className="pcoded-main-container col-sm-12">
                    <div className="pcoded-content col-sm-12">
                        <div className="col-sm-12">
                            <div className="card">

                            <div className="card-body ml-2">
                                <form>
                                    <div className="form-group col-sm-12">
                                        <h2 className="mt-5">Agregar Nuevo Producto</h2>
                                    </div>
                                    <div className="form-group col-sm-12">
                                        <label htmlFor="categoria">Categoria</label>
                                        <input type="text" className="form-control"id="categoria" value={categoria} onChange={(e)=>{setCategoria(e.target.value)}} placeholder="Categoria"></input>
                                    </div>
                                    <div className="form-group col-sm-12">
                                        <label htmlFor="nombre">Nombre</label>
                                        <input type="text" className="form-control" id="nombre" value={nombre} onChange={(e)=>{setNombre(e.target.value)}} placeholder="Nombre"></input>
                                    </div>
                                    <div className="form-group col-sm-12">
                                        <label htmlFor="nombre">Marca</label>
                                        <input type="text" className="form-control" id="nombre" value={marca} onChange={(e)=>{setMarca(e.target.value)}} placeholder="Nombre"></input>
                                    </div>
                                    <div className="form-group col-sm-12">
                                        <label htmlFor="precio">Precio</label>
                                        <input type="text" className="form-control" id="precio" value={precio} onChange={(e)=>{setPrecio(e.target.value)}}  placeholder="Precio"></input>
                                    </div>
                                    <div className="form-group col-sm-12">
                                        <label htmlFor="precio">Imagen</label>
                                        <input type="text" className="form-control" id="imagen" value={imagen} onChange={(e)=>{setImagen(e.target.value)}}  placeholder="Direccion De Imagen"></input>
                                    </div>
                                    <div className="form-group col-sm-12">
                                        <label htmlFor="activo">Stock</label>
                                        <input type="text" className="form-control" id="activo" value={estado} onChange={(e)=>{setEstado(e.target.value)}} placeholder="Activo"></input>
                                    </div>
                                    <div className="mb-12">
                                        <button type="button" onClick={ regresar } className="btn btn-primary">Regresar</button>
                                        <button type="button" onClick={ agregarProducto } className="btn btn-success">Agregar</button>
                                    </div>
                                </form>
                            </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            </div>
        </div>
        </div>
    )
}

export default AgregarProducto;

/*
    <div className="container mt-5">
            <h4>Nuevo Producto</h4>
            <div className="row">
                <div className="col-md-12">
                <div className="mb-3">
                        <label htmlFor="categoria" className="form-label">Categoria</label>
                        <input type="text" className="form-control" id="categoria" value={categoria} onChange={(e) => {setCategoria(e.target.value)}}></input>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="nombre" className="form-label">Nombre</label>
                        <input type="text" className="form-control" id="nombre" value={nombre} onChange={(e) => {setNombre(e.target.value)}}></input>
                    </div>                
                    <div className="mb-3">
                        <label htmlFor="marca" className="form-label">Marca</label>
                        <input type="text" className="form-control" id="marca" value={marca} onChange={(e) => {setMarca(e.target.value)}}></input>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="precio" className="form-label">Precio</label>
                        <input type="text" className="form-control" id="precio" value={precio} onChange={(e) => {setPrecio(e.target.value)}}></input>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="imagen" className="form-label">Imagen</label>
                        <input type="text" className="form-control" id="imagen" value={imagen} onChange={(e) => {setImagen(e.target.value)}}></input>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="estado" className="form-label">Stock</label>
                        <input type="text" className="form-control" id="estado" value={estado} onChange={(e) => {setEstado(e.target.value)}}></input>
                    </div>              
                    <div className="mb-12">
                        <button type="button" onClick={ regresar } className="btn btn-primary">Regresar</button>
                        <button type="button" onClick={ agregarProducto } className="btn btn-success">Agregar</button>
                    </div>
                </div>
            </div>
        </div>
*/