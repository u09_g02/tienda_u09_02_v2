import { Link, useNavigate } from 'react-router-dom';
import { useState } from 'react';
import APIInvoke from '../utils/APIInvoke.js';
import swal from 'sweetalert'

function Login(){

    const navigate = useNavigate();
    const [username, setUserName] = useState('');
    const [password, setPassword] = useState('');

    const iniciarSesion = async () =>{

        if(password.length < 8){
            const msg = "La contraseña debe contener almenos 8 caracteres";
            swal({
                title: 'Error',
					text: msg,
					icon: 'error',
					buttons: {
						confirm: {
							text: 'Ok',
							value: true,
							visible: true,
							className: 'btn btn-danger',
							closeModal: true
						}
					}
				});
        } else{
            const data ={
                userName: username,
                email: username,
                password: password,
            }

            const response = await APIInvoke.invokePOST(`/api/v1/auth/`, data);

            console.log(response);

            const mensaje = response.msg;
            console.log(mensaje);

            if (mensaje === 'El usuario no existe' || mensaje === 'Contraseña incorrecta') {
                const msg = "No fue posible iniciar la sesión verifique los datos ingresados.";
                swal({
                    title: 'Error',
                    text: msg,
                    icon: 'error',
                    buttons: {
                        confirm: {
                            text: 'Ok',
                            value: true,
                            visible: true,
                            className: 'btn btn-danger',
                            closeModal: true
                        }
                    }
                });
            } else{
                //obtenemos el token de acceso jwt
                const jwt = response.token;

                //guardamos el token en el localstorage
                localStorage.setItem('token', jwt);

                //redireccionamos al home la pagina principal
                navigate("/menu");
            }
        }
    }

    const onSubmit = (e) => {
        e.preventDefault();
        iniciarSesion();
    }

    return(
        <div className="Login">
        <section className="login p-fixed d-flex text-center bg-primary common-img-bg">
            <div className="container-fluid">
                <div className="row">
                    <div className="col-sm-12">
                        <div className="login-card card-block">
                            <form className="md-float-material" onSubmit={ onSubmit }>
                                <div className="text-center">
                                    <img src="assets/images/logouismisiontic.png" alt="logo"></img>
                                </div>
                                <h3 className="text-center txt-primary">
                                    Iniciar sesión
                                </h3>
                                <div className="row">
                                    <div className="col-md-12">
                                    <div className="md-input-wrapper">
                                            <input type="text" className="md-form-control" required autoComplete="on" value={ username } onChange={(e) => {setUserName(e.target.value)}}></input>
                                            <label>Nombre de Ususario o Email</label>
                                        </div>

                                    </div>
                                    <div className="col-md-12">
                                        <div className="md-input-wrapper">
                                            <input type="password" className="md-form-control" autoComplete="off" required value={ password } onChange={(e) => {setPassword(e.target.value)}}></input>
                                            <label>Password</label>
                                        </div>
                                    </div>
                                    <div className="col-sm-6 col-xs-12">
                                    <div className="rkmd-checkbox checkbox-rotate checkbox-ripple m-b-25">
                                        <label className="input-checkbox checkbox-primary">
                                            <input type="checkbox" id="checkbox"></input>
                                            <span className="checkbox"></span>
                                        </label>
                                        <div className="captions">Recordar usuario</div>
                                    </div>
                                        </div>
                                    <div className="col-sm-6 col-xs-12 forgot-phone text-right">
                                        <a href="forgot-password.html" className="text-right f-w-600">Olvidaste el Password?</a>
                                        </div>
                                </div>
                                <div className="row">
                                    <div className="col-xs-10 offset-xs-1">
                                        <button onClick={ iniciarSesion } className="btn btn-primary btn-md btn-block waves-effect text-center m-b-20">
                                            Ingresar
                                        </button>
                                    </div>
                                </div>
                                <div className="card-footer">
                                <div className="col-sm-12 col-xs-12 text-center">
                                    <span className="text-muted">No tienes cuenta?</span>
                                    <Link to={"/register"} className="btn btn-block btn-danger">
                                            Crear Cuenta
                                    </Link>
                                </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    )
}

export default Login;