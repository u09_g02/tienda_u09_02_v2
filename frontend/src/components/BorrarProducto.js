import axios from "axios";
import Swal from 'sweetalert2';
import { useNavigate } from 'react-router-dom';

function BorrarProducto(id){

    const navigate = useNavigate();

    const direccion ='https://tienda-u09-02-v2.herokuapp.com/api/v1/productos/borrar';

    // Peticion para autenticar
    // **************************************************************************************
    const token = localStorage.getItem('token');
    let bearer;
    if(token===''){
      bearer="";
    } else{
      bearer= bearer=`${token}`;
    }
    const config={headers:{'Content-Type': 'application/json','x-auth-token': bearer}}
    // **************************************************************************************

    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
          confirmButton: 'btn btn-success',
          cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
      })
      
      swalWithBootstrapButtons.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        reverseButtons: true
      }).then((result) => {
        if (result.isConfirmed) {

            //Metodo borrar
            axios.delete(`${direccion}/${id}`, config).then(res => {
              console.log("");
            }).catch(err => { console.log(err.stack) });

            swalWithBootstrapButtons.fire(
            'Deleted!',
            'Your file has been deleted.',
            'success'
          )
          navigate('/menu');
        } else if (
          /* Read more about handling dismissals below */
          result.dismiss === Swal.DismissReason.cancel
        ) {
          swalWithBootstrapButtons.fire(
            'Cancelled',
            'Your imaginary file is safe :)',
            'error'
          )
        }
      })
}

export default BorrarProducto;