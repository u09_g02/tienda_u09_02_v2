// import { Link } from 'react-router-dom';
import Encabezado from './Encabezado.jsx';
import MenuLateral from './MenuLateral.jsx';
import ProductoListar from './ProductoListar.jsx';

function Menu() {
  return (
    <div className="sidebar-mini fixed">
        <div className="wrapper">
            <Encabezado/>
            <MenuLateral/>
            <ProductoListar/>
        </div>
    </div>

  );
}

export default Menu;
