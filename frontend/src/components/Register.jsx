import { Link, useNavigate } from 'react-router-dom';
import { useState } from 'react';
import swal from 'sweetalert';
import Swal from 'sweetalert2';
import uniquid from 'uniqid';
import axios from 'axios';

function Register(){

    const navigate = useNavigate();

    const [userName, setUserName] = useState('');
    const [password, setPassword] = useState('');
    const [passwordS, setPasswordS] = useState('');
    const [email, setEmail] = useState('');

    function crearCuenta(){
        const nuevoUsusario ={
            id: uniquid(),
            userName: userName,
            password: password,
            email: email
        }

        if(password === passwordS){
            axios.post('https://tienda-u09-02-v2.herokuapp.com/api/v1/usuarios/', nuevoUsusario).then(res =>{
                Swal.fire({ position: 'center', icon: 'success',  title: 'El Ususario ha sido registrado con exito!', showConfirmButton: false, timer: 1500 });
                navigate('/');
            }).catch(err => {
                console.log(err.stack);
            })
        } else{
            const msg = "Las contraseñas no coinciden!";
            swal({
                title: 'Error',
					text: msg,
					icon: 'error',
					buttons: {
						confirm: {
							text: 'Ok',
							value: true,
							visible: true,
							className: 'btn btn-danger',
							closeModal: true
						}
					}
				});
        }
    }

    const onSubmit = (e) => {
        e.preventDefault();
    }

    return (
        <div className="Register">
        <section className="login common-img-bg">
            <div className="container-fluid">
                <div className="row">
                        <div className="col-sm-12">
                            <div className="login-card card-block bg-white">
                                <form className="md-float-material" onSubmit={ onSubmit }>
                                    <div className="text-center">
                                        <img src="assets/images/logouismisiontic.png" alt="logo"></img>
                                    </div>
                                    <h3 className="text-center txt-primary">Crear una cuenta</h3>
                                    <div className="md-input-wrapper">
                                        <input type="text" id="userName" className="md-form-control" autoComplete="on" required value={ userName } onChange={(e) => {setUserName(e.target.value)}}></input>
                                        <label>UserName</label>
                                    </div>

                                    <div className="md-input-wrapper">
                                        <input type="email" id="email" className="md-form-control" autoComplete="on" required value={ email } onChange={(e) => {setEmail(e.target.value)}}></input>
                                        <label>Email</label>
                                    </div>

                                    <div className="md-input-wrapper">
                                        <input type="password" id="password" className="md-form-control" autoComplete="off" required value={ password } onChange={(e) => {setPassword(e.target.value)}}></input>
                                        <label>Password</label>
                                    </div>
                                    
                                    <div className="md-input-wrapper">
                                        <input type="password" id="passwordS" className="md-form-control" autoComplete="off" required value={ passwordS } onChange={(e) => {setPasswordS(e.target.value)}}></input>
                                        <label>Repetir Password</label>
                                    </div>
                                    <div className="rkmd-checkbox checkbox-rotate checkbox-ripple b-none m-b-20">
                                        <label className="input-checkbox checkbox-primary">
                                            <input type="checkbox" id="checkbox"></input>
                                            <span className="checkbox"></span>
                                        </label>
                                        <div className="captions">Recordarme</div>
                                    </div>
                                    <div className="col-xs-10 offset-xs-1">
                                        <button type="submit" className="btn btn-primary btn-md btn-block waves-effect waves-light m-b-20" onClick={ crearCuenta }>Registrarme</button>
                                    </div>
                                    <div className="row">
                                        <div className="col-xs-12 text-center">
                                            <span className="text-muted">Ya tienes una cuenta?</span>
                                            <Link to={"/"} className="f-w-600 p-l-5">
                                                Iniciar sesión
                                            </Link>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
        </section>
        </div>
    )
}

export default Register;