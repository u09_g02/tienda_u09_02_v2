const modeloCliente = require('../models/model_clientes.js');
const modeloEncabezadoFactura = require('../models/model_encabezado_facturas');

const miconexion = require('../conexion');

var dataCliente = [];
modeloCliente.find({ id:'1' }).then((data) => {
    console.log("Datos del cliente: ");
    console.log(data);
    data.map((k, v) => {
        // Guardamos como llave el id del cliente en el array
        dataCliente.push(k.id);
    });
    // Apartir de las llaves guardada en el array buscamos las facturas con ese identificador y las mostramos
    modeloEncabezadoFactura.find({ id_cliente: { $in: dataCliente } })
    .then((data) => {
        console.log("Encabezado de facturas del clinte: ");
        console.log(data);
    })
    .catch((err) => { console.log(err); });
});


modeloCliente.aggregate([{
    $lookup: {
        from: 'encabezado_facturas',
        localField: 'id',
        foreignField: 'id_cliente',
        as: 'encabezado_facturas'
    },
},
{
    $unionWith: '$encabezado_facturas'
}]).then(docs => { res.send(docs); console.log(docs); } )
.catch(err => { res.send(err) });