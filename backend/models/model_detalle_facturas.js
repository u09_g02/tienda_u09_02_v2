const mongoose = require('mongoose');
const miesquema = mongoose.Schema;

const productoSchema = new miesquema({
    id_producto: String,
    cantidad: Number,
    total: Number
})

const detalleFacturaSchema = new miesquema({
    id_encabezado_factura: String,
    productos: [{type: productoSchema}]
},
{
    timestamps: true,
    versionKey: false
});

const modelo_detalle_factura = mongoose.model('detalle_facturas', detalleFacturaSchema);

module.exports = modelo_detalle_factura;