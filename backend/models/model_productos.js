const mongoose = require ('mongoose');
const miesquema = mongoose.Schema;
const esquemaProductos = new miesquema({
    id : String,
    categoria : String,
    nombre : String,
    marca : String,
    precio : Number,
    imagen : String,
    estado : Boolean
});

const modeloProductos = mongoose.model('productos', esquemaProductos);

module.exports = modeloProductos;
