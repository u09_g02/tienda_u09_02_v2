const mongoose = require('mongoose');
const miesquema = mongoose.Schema;

const usuarioEsquema = new miesquema({
    id: {type: String, required : true, trim: true},
    userName: {type: String, required : true, trim: true, unique: true},
    password: {type: String, required : true, trim: true},
    email: {type: String, required : true, trim: true, unique: true}
},
{
    timestamps: true,
    versionKey: false
});

const modeloUsuario = mongoose.model('usuarios', usuarioEsquema);

module.exports = modeloUsuario;