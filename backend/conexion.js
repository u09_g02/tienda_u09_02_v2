const mongoose = require('mongoose');
require("dotenv").config({ path: "variables.env" });

const URL = process.env.DB_MONGO;  //Cambiar nombre de la BD

mongoose.connect(URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    //useCreateIndex: true,
    //useFindAndModify: false
});

const miconexion = mongoose.connection;

miconexion.on('connected', () => {
    console.log("La conexion con la BD fue exitosa");
});

miconexion.on('error', () => {
    console.log("Error: No se pudo establecer la conexion con la BD");
});

module.exports = mongoose;