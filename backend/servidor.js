require("dotenv").config({ path: "variables.env" });
const cors = require('cors');
const express = require('express');
const routers = require('./routers/routers.js');
const PORT = process.env.PORT || 4000; 

const app = express();
app.use(cors());

// conection con la BD
const miconexionBD = require('./conexion');

// Middleware(validar contenido) --> invocacion del bodyParser -- para archivos JSON
const bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true })); //soluciona el problema de acentos y caracteres especiales de espanol

// rutas
app.use('/api/v1', routers);

app.get('/', function(req,res){
    res.send('<h1>El Back-End esta funcionando</h1>')
});

// Se utilizara el puerto 5000
app.listen(PORT, 
    function(){
        console.log(`Servidor corriendo OK! en el puerto ${PORT} - http://localhost:${PORT}`);
});