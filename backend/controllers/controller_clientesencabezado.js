const express = require('express');
const router = express.Router();
const modeloEncabezadoFacturas = require('../models/model_encabezado_facturas');
const modeloClientes = require('../models/model_clientes');

router.get('/encabezadoclientes', (req, res) => {
    modeloClientes.aggregate([
        {
                $lookup: {
                localField: "id",
                from: "encabezado_factura",
                foreignField: "id_cliente",
                as: "encabezado_factura_clientes",
            },
        },
        { $unwind: "$encabezado_factura_clientes" }])
        .then((result)=>{res.send(result);console.log(result)})
        .catch((error)=>{res.send(error);console.log(error)});
    });

    router.get('/encabezadoclientes/:id', (req, res) => {
            var dataClientes = [];
    modeloClientes.find({id:req.params.id}).then(data => {
        console.log("Datos del Cliente:");
        console.log(data);
        data.map((d, k) => {dataClientes.push(d.id);})
        modeloEncabezadoFacturas.find({id_cliente: { $in: dataClientes}})
        .then(data => {
            console.log("Pedidos del cliente:");
            console.log(data);
            res.send(data);
        })
        .catch((error)=>{res.send(error);console.log(error)});
        });
    });

    module.exports = router;
