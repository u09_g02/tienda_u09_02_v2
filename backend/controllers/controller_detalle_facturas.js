const modeloDetalleFactura = require('../models/model_detalle_facturas.js');
const express = require('express');
const router = express.Router();

router.get('/listar', (req, res) =>{
    modeloDetalleFactura.find({}, function(docs, err){
        if(!err){
            res.send(docs);
        } else{
            res.send(err);
        }
    });
});

router.get('/cargar/:id', (req, res) => {
    modeloDetalleFactura.findById({ _id: req.params.id }, function(docs, err){
        if(!err){
            res.send(docs);
        } else{
            res.send(err);
        }
    });
});

router.post('/agregar', (req, res) => {
    const { id_encabezado_factura, productos } = req.body;
    const newDetalleFactura = new modeloDetalleFactura({
        id_encabezado_factura,
        productos
    });
    newDetalleFactura.save((err) => {
        if(!err){
            res.send("Se agrego el detalle de una factura");
        } else{
            res.send(`Error: ${err}`);
        }
    });
});

router.put('/editar/:id', (req, res) => {
    const { id_encabezado_factura } = req.body;
    const { id_productos, cantidad, total } = req.body;
    modeloDetalleFactura.findOneAndUpdate({ id_encabezado_factura: req.params.id },
    {
        id_encabezado_factura,
        productos : [{
            id_productos,
            cantidad,
            total
        }]
    },
    (err) => {
        if(!err){
            res.send("El resgistro se ha actualizado con exito");
        } else{
            res.send(`Error: ${err}`);
        }
    });

});

router.delete('/borrar/:id', (req, res) => {
    modeloDetalleFactura.findOneAndDelete({ id_encabezado_factura: req.params.id }, (err) => {
        if(!err){
            res.send("El elemento se ha borrado con exito");
        } else{
            res.send(`Error: ${err}`);
        }
    });
});

module.exports = router;