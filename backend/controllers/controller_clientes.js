const modeloClientes = require('../models/model_clientes.js');
const express = require('express');
const router = express.Router();


//  /listar

router.get('/listar', (req, res) => {
    modeloClientes.find({}, function(docs, err){
        if(!err){
            res.send(docs);
        } else{
            res.send(err);
        }
    })
});

//
router.get('/cargar/:id', (req, res) => {
    modeloClientes.find({id: req.params.id}, function(docs, err){
        if(!err){
            res.send(docs);
        } else{
            res.send(err);
        }
    })
});

router.get('/findByUserName/:userName', (req, res) => {
    modeloClientes.findOne({userName: req.params.userName}, function(docs, err){
        if(!err){
            res.send(docs);
        } else{
            res.send(err);
        }
    })
});

router.post("/agregar", (req, res) => {
    const { id, userName, contrasena, correo, nombre, telefono, fechaNacimiento, pais, direccion } = req.body;
    const newCliente = new modeloClientes({
        id,
        userName,
        contrasena,
        correo,
        nombre,
        telefono,
        fechaNacimiento,
        pais,
        direccion
    });
    newCliente.save(function(err){
        if(!err){
            res.send("Se ha creado un nuevo cliente de forma saftisfactoria")
        } else{
            res.send(`Èrror: ${err}`);
        }
    }) 
});

router.put('/editar/:id', (req, res) => {
    const { userName, contrasena, correo, nombre, fechaNacimiento, pais, direccion } = req.body;
    modeloClientes.findOneAndUpdate({ id: req.params.id },
    {
        userName,
        contrasena,
        correo,
        nombre, 
        fechaNacimiento,
        pais,
        direccion
    }),
    (err) =>{
        if(!err){
            res.send("Los datos del cliente han sido actualizados");
        } else{
            res.send(`Error: ${err}`);
        }
    }
});

router.delete("/borrar/:id", (req, res) => {
    modeloClientes.findOne({id: req.params.id}, function(err){
        if(!err){
            res.send("El Cliente ha sido eliminado con exito");
        } else{
            res.send(`Error: ${err}`);
        }
    });
});

module.exports = router;