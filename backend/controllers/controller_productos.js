const express = require('express');
const router = express.Router();

const modeloProductos = require('../models/model_productos');

router.get('/listar', (req, res) => {
    modeloProductos.find({},function(docs,err)
    {
        if(!err)
        {
            res.send(docs);
        }
        else
        {
            res.send(err)
        }
    })
});


router.get('/cargar/:id', (req, res) => {
    modeloProductos.find({id:req.params.id},function(docs,err)
    {
        if(!err)
        {
            res.send(docs);
        }
        else
        {
            res.send(err)
        }
    })
});

router.post('/agregar', (req,res) =>{
    const nuevoProducto = new modeloProductos({
        id : req.body.id,
        categoria : req.body.categoria,
        nombre : req.body.nombre,
        marca : req.body.marca,
        precio : req.body.precio,
        estado : req.body.estado
    });

    nuevoProducto.save(function(err)
    {
        if(!err)
        {
            res.send("El producto fue agregado exitosamente.");
        }
        else
        {
            res.send(err.stack);
        }
    });
});



router.post('/editar/:id', (req,res) =>{
    modeloProductos.findOneAndUpdate({id:req.params.id},
        {
            categoria : req.body.categoria,
            nombre : req.body.nombre,
            marca : req.body.marca,
            precio : req.body.precio,
            estado : req.body.estado
        }, (err) =>
        {
            if(!err)
            {
                res.send("El registro fue modificado.");
            }
            else
            {
                res.send(err.stack);
            }
        }
    )
});

router.delete('/borrar/:id', (req, res) =>{
    modeloProductos.findOneAndDelete({id:req.params.id},
        (err) =>
        {
            if(!err)
            {
                res.send("El producto fue eliminado.");
            }
            else
            {
                res.send(err.stack);
            }
        }
    )
});

module.exports = router;