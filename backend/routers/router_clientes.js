const express = require('express');
const router = express.Router();
const controllerClientes = require('../controllers/controller_clientes.js');
const auth = require('../middleware/auth');

router.get('/listar', auth, controllerClientes);
router.get('/cargar/:id', auth, controllerClientes);
router.get('/findByUserName/:userName', auth, controllerClientes);
router.put('/editar/:id', auth, controllerClientes);
router.post('/agregar', auth, controllerClientes);
router.delete('/borrar/:id', auth, controllerClientes);

module.exports = router;