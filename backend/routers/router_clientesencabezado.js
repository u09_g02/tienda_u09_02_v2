const express = require('express');
const router = express.Router();

const controladorEncabezadoClientes = require('../controllers/controller_clientesencabezado');
router.get("/encabezadoclientes",controladorEncabezadoClientes);
router.get("/encabezadoclientes/:id",controladorEncabezadoClientes);



module.exports = router;

