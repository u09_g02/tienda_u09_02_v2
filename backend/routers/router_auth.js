//Rutas para autenticar usuarios

const express = require("express");
//Rutas para autenticar usuarios

const express = require("express");
const router = express.Router();
const { check } = require("express-validator");
const authController = require("../controllers/controller_auth.js");
const auth = require("../middleware/auth");

// Autentica un usuario
// api/auth
router.post(
  "/",
  [
    check("userName", "El nombre es obligatorio").not().isEmpty(),
    check("email", "Error: Agrega un email válido").not().isEmpty(),
    check("password", "El password debe ser mínimo de 8 caracteres").isLength({ min: 8, })
  ],
  authController.autenticarUsuario
);

router.get('/', auth, authController.usuarioAutenticado);

module.exports = router;

