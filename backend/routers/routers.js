const express = require('express');
const router = express.Router();

const routerAuth = require('./router_auth.js');
router.use('/auth', routerAuth);

const routerProductos = require('./router_productos.js');
router.use('/productos', routerProductos);

const routerClientes = require('./router_clientes');
router.use('/clientes', routerClientes);

const routerEncabezadoFacturas = require('./router_encabezado_facturas.js');
router.use('/encabezado_facturas', routerEncabezadoFacturas);

const routerDetalleFacturas = require('./router_detalle_facturas.js');
router.use('/detalle_facturas', routerDetalleFacturas);

const routerUsuarios = require('./router_usuarios.js');
router.use('/usuarios', routerUsuarios);

module.exports = router;